# Descripcion
Convierte el excel que Insside usa para GAP analisis en una estructura de arbol general

# Consideraciones
Validar que el excel tenga las celdas bien convinadas para la colummna 1, en caso contrario, convinarlas usando MS Excel.
Para el excel del repo se convinaron las siguientes celdas
- 146 y 147
- 158 y 159
- 264 y 265
- 397 y 398

# Ejecución

Se necesita tener Nodejs instalado en la maquina

*  Instalar dependencias

```bash
    npm install
``` 
* Ejecutar

```bash
    npm start
``` 

