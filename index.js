var exceljs = require('exceljs');
var fs = require('fs');

function GAP(data) {
    this.version = "3.2"
    this.requisitos = []
}

function Requisito(nro, titulo) {
    this.nro = nro
    this.titulo = titulo
    this.secciones = []
}

function Seccion(codigo, titulo, guia) {
    this.codigo = codigo
    this.titulo = titulo
    this.guia = guia
    this.pruebas = []
}

function Prueba(codigo, texto) {
    this.codigo = codigo
    this.texto = texto
}

var workbook = new exceljs.Workbook();
workbook.xlsx.readFile("SAQ_Modif.xlsx")
    .then(function (file) {
        var root = process(file.getWorksheet());
        fs.writeFile("/tmp/root.json", JSON.stringify(root), function(err) {
            if(err) {
                return console.log(err);
            }
        
            console.log("arbol salvado!");
        });
        //salvar root a un mongo o algo similar
    })
    .catch((err) => console.log(err));

var process = function (worksheet) {

    var gap = new GAP();

    var i = 2, nroRequisito = 1;
    var row = worksheet.getRow(i);

    var requisito;
    while (row.getCell(1).value) {
        if (/^((Requisito)\s[0-9]*(:)\s)/.test(row.getCell(1).value)) {

            requisito = new Requisito(nroRequisito, row.getCell(1).value);
            console.log("+", requisito.titulo);
            gap.requisitos.push(requisito);
            nroRequisito++;
            // salto la fila de los headers que viene luego de cada requisito
            i = i + 2;

        } else if (/^(Para aplicaciones web e interfaces de aplicaciones)/.test(row.getCell(1).text)) {
            // Descarto la fila
            i++;
        } else {
            
            let seccion = new Seccion(row.getCell(1).text.substr(0, row.getCell(1).text.indexOf(' ')), row.getCell(1).text, row.getCell(3).text)
            console.log("+", seccion.titulo);
            
            for (let inx = 0; inx <= row.getCell(1)._mergeCount; inx++) {
                let textoDePrueba = worksheet.getCell(row.number + inx, 2).text;
                let prueba = new Prueba(textoDePrueba.substr(0, textoDePrueba.indexOf(' ')), textoDePrueba)
                seccion.pruebas.push(prueba);
            }
            requisito.secciones.push(seccion)

            // avanzo una fila y sumo las filas combinadas del row actual
            i = i + row.getCell(1)._mergeCount + 1;
        }

        row = worksheet.getRow(i);
    }

    return gap;
}